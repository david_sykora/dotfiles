## Dependencies
### Desktop
    - bat
    - pistol
    - lf
    - xcopy
    - python
    - vim
    - wget / curl
### Desktop
    - python
    - vim
    - wget / curl
## Installation
### Desktop
`./install -c install-desktop.conf.yaml`
### Server
`./install -c install-server.conf.yaml`
