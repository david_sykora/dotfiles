" PLUGINS
call plug#begin('~/.vim/plugged')

syntax enable
"set background=dark
"colorscheme solarized

Plug '~/.zplug/repos/junegunn/fzf'
Plug 'junegunn/fzf.vim'

call plug#end()

" SYNTAX
syntax on " highlighting

" INDENTATION
filetype plugin indent on
set tabstop=4 " show existing tab with 4 spaces width
set shiftwidth=4 " when indenting with '>', use 4 spaces width
set expandtab " On pressing tab, insert 4 spaces
set paste

" LINE NUMBERS
set number

" KEYMAPS
let mapleader=" "
nnoremap <silent> <leader>o :Files<CR>
nnoremap <silent> <leader>O :Files!<CR>

let g:fzf_action = {'alt-c': "call fzf#run(fzf#wrap({ 'dir': '..' }))\"",
\ 'ctrl-t': 'tab split',
\ 'ctrl-x': 'split',
\ 'ctrl-v': 'vsplit' }
