# ZPLUG init
source ~/.zplug/init.zsh

zplug "zsh-users/zsh-completions"
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug "junegunn/fzf", from:github, as:command, rename-to:fzf, hook-build:"./install --all"
zplug "junegunn/fzf", from:github, as:plugin, use:"shell/*.zsh"
zplug "plugins/ssh-agent",   from:oh-my-zsh

# load custom completions
compinit
#fpath=(~/.zsh/completion $fpath)
fpath=($fpath)

# zplug install
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load

# completation select via menu
zstyle ':completion:*' menu yes select

# PROMPT
PROMPT='%n@%m:%~%% '

# EDIT MODE
bindkey -e
autoload -U select-word-style
select-word-style bash

# FZF preview
export FZF_DEFAULT_OPTS="--height 100%"
if [ -f /usr/bin/bat ]; then
  export FZF_CTRL_T_OPTS="--height 100% --preview 'bat --color=always {}'"
else
  export FZF_CTRL_T_OPTS="--height 100% --preview 'cat {}'"
fi
# export FZF_COMPLETION_OPTS="--preview 'bat --style=numbers --color=always {}'"
# export FZF_CTRL_T_OPTS="--preview 'bat --style=numbers --color=always {}'"
#export FZF_ALT_C_OPTS="--preview 'ls -lah --color {}'"

# SHORTCUTS

WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'

case "${TERM}" in
  cons25*|linux) # plain BSD/Linux console
    bindkey '\e[H'    beginning-of-line   # home
    bindkey '\e[F'    end-of-line         # end
    bindkey '\e[5~'   delete-char         # delete
    bindkey '^[^[[D'      emacs-backward-word # esc left
    bindkey '^[[1;5C'      emacs-forward-word  # esc right
    ;;
  *rxvt*) # rxvt derivatives
    bindkey '\e[3~'   delete-char         # delete
    bindkey '^[[C'    forward-word        # ctrl right
    bindkey '^[[D'    backward-word       # ctrl left
    # workaround for screen + urxvt
    bindkey '\e[7~'   beginning-of-line   # home
    bindkey '\e[8~'   end-of-line         # end
    bindkey '^[[1~'   beginning-of-line   # home
    bindkey '^[[4~'   end-of-line         # end
    ;;
  alacritty) 
    bindkey '\e[3~'   delete-char         # delete
    bindkey '^[[1;5C'    forward-word        # ctrl right
    bindkey '^[[1;5D'    backward-word       # ctrl left
    # workaround for screen + urxvt
    bindkey '\e[7~'   beginning-of-line   # home
    bindkey '\e[8~'   end-of-line         # end
    bindkey '^[[1~'   beginning-of-line   # home
    bindkey '^[[4~'   end-of-line         # end
    ;;
  *xterm*) # xterm derivatives
    bindkey '\e[H'    beginning-of-line   # home
    bindkey '\e[F'    end-of-line         # end
    bindkey '\e[3~'   delete-char         # delete
    bindkey '^[[1;5C' forward-word        # ctrl right
    bindkey '^[[1;5D' backward-word       # ctrl left
    # workaround for screen + xterm
    bindkey '\e[1~'   beginning-of-line   # home
    bindkey '\e[4~'   end-of-line         # end
    ;;
  screen)
    bindkey '^[[1~'   beginning-of-line   # home
    bindkey '^[[4~'   end-of-line         # end
    bindkey '\e[3~'   delete-char         # delete
    bindkey '\eOc'    forward-word        # ctrl right
    bindkey '\eOd'    backward-word       # ctrl left
    bindkey '^[[1;5C' forward-word        # ctrl right
    bindkey '^[[1;5D' backward-word       # ctrl left
    ;;
  st-256color)
    bindkey '^[[1;5D' backward-word
    bindkey '^[[1;5C' forward-word
esac

##############################################################################
## History Configuration
###############################################################################
HISTSIZE=500000               #How many lines of history to keep in memory
HISTFILE=~/.zsh_history     #Where to save history to disk
SAVEHIST=500000               #Number of history entries to save to disk
HISTDUP=erase               #Erase duplicates in the history file
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt append_history     #Append history to the history file (no overwriting)
setopt share_history      #Share history across terminals
setopt inc_append_history  #Immediately append to the history file, not just when a term is killed

export PATH=$PATH:~/.cargo/bin
export PATH=$PATH:~/.local/bin
export EDITOR=/usr/bin/vim

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

### SSH-AGENT
SSH_ENV="$HOME/.ssh/environment"

function start_agent {
    echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    echo succeeded
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add;
}

# Load user profile file
if [ -f ~/.profile ]; then
  . ~/.profile
fi

### Aliases
# TODO: solve breaking completion
#alias ip="ip --color"
alias ls="ls --color"
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'
export _JAVA_AWT_WM_NONREPARENTING=1
export VAGRANT_HOME=/mnt/WD2TB/vagrant;
export MINIKUBE_HOME=/mnt/WD2TB/minikube;
export TMPDIR=/mnt/WD2TB/vagrant-images;


# tabtab source for packages
# uninstall by removing these lines
[[ -f ~/.config/tabtab/zsh/__tabtab.zsh ]] && . ~/.config/tabtab/zsh/__tabtab.zsh || true
source ~/.skaffold-completion
